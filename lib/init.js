import axios from 'axios';
import {username, password} from './config.json';

const bb = 'https://api.bitbucket.org/2.0';

const done = [];
async function get (url) {
	const res = await axios.get(url, {
		auth: {
			username,
			password
		}
	});
    if (!res.data.next) {
		return res.data.values;
    } else if (!done[res.data.next]) {
        done[res.data.next] = true;
		return res.data.values.concat(await get(url));
	} else {
		return [];
	}
}

async function others() {
    let repos = await get(`${bb}/repositories?role=member`);
    repos = repos.filter(repo => repo.owner.username != username.slice(0, username.indexOf('@')))
        .filter(repo => repo.project == null)
        .map(repo => {
            const nproj = {
                name: repo.name,
                fullName: repo.full_name,
                url: repo.links.html.href
            };
            nproj[repo.links.clone[0].name] = repo.links.clone[0].href
            nproj[repo.links.clone[1].name] = repo.links.clone[1].href
            return nproj;
        });
    return {
        name: 'other',
        projects: repos
    }
};

async function init () {
	const teams = await get(`${bb}/teams?role=member`);
	let teamData = teams.map(a=>{return {
		name: a.display_name,
		repos: a.links.repositories.href,
		projects: []
	}});
	return Promise.all(teamData.map(async team => {
        return {
			name: team.name,
			projects: (await get(team.repos)).map(proj => {
				const nproj = {
					name: proj.name,
					fullName: proj.project.name,
					url: proj.project.links.html.href
				}
				nproj[proj.links.clone[0].name] = proj.links.clone[0].href
				nproj[proj.links.clone[1].name] = proj.links.clone[1].href
				return nproj;
			})
		}
	}).concat([others()]));
}

module.exports = init;
