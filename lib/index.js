import 'babel-polyfill';
import init from './init';
import blessed from 'blessed';
import contrib from 'blessed-contrib';
import {log, createLogBox} from './logBox.js';
import {createTeamList, updateTeamList} from './teamList.js';
import {createRepos} from './repos.js';

try {
let teams;
const screen = blessed.screen({smartCSR: true, dockBorders: true});
screen.title = 'Grader';
const logBox = createLogBox(screen);
const header = [['Name','Full name','Url', 'SSH', 'HTTP']];
const repos = createRepos(screen, header);
const teamList = createTeamList(screen, header);

screen.key(['C-c'], (ch, key) => {
    screen.destroy();
    return process.exit(0);
});
screen.key(['linefeed', 'C-j'], (ch, key) => {
    screen.focusPrev();
});
screen.key('C-k', (ch, key) => {
    screen.focusNext();
});
screen.on('element focus', (elem) => {
    elem.style.border.fg = 'white';
    screen.render();
});
screen.on('element blur', (elem) => {
    elem.style.border.fg = 'magenta';
    screen.render();
});

(async () => {
    teams = await init();
    updateTeamList(teams);
    teamList.setItems(teams.map(teams => teams.name));
    teamList.focus();
    //log(teams);
    screen.render();
})();

} catch (e) {
    log(e);
}
