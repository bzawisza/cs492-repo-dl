import blessed from 'blessed';
import {getRepos} from './repos';

let teamList;
let teams;
export function createTeamList(screen, header) {
    if (!teamList)
        create(screen, header);
    return teamList;
}

export function getTeamList() {
    return teamList;
}

export function updateTeamList(t) {
    teams = t;
}

function create(screen, header) {
    teamList = blessed.list({
        parent: screen,
        top: '0',
        left: '0',
        width: '15%',
        height: '50%',
        tags: true,
        scrollable: true,
        border: {
            type: 'line',
            left:true,
            right:true,
            top:true,
            bottom:true
        },
        keys: true,
        vi: true,
        label: 'Teams',
        style: {
            selected: {
                bg: 'green'
            },
            item: {
                fg: 'white',
                bg: 'black',
            },
            border: {
                fg: 'magenta'
            }
        },
        items: []
    });

    teamList.on('select', (obj,id) => {
        const repos = getRepos();
        const table = [...header, ...teams[id].projects.map(proj => {
            const row = [];
            row.push(proj.name)
            row.push(proj.fullName)
            row.push(proj.url)
            row.push(proj.https)
            row.push(proj.ssh)
            return row;
        })];
        repos.setData(table);
        repos.focus();
        screen.render();
    });
}
