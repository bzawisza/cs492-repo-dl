import blessed from 'blessed';
import {log} from './logBox.js'
import {exec} from 'child_process';
import {username, password, compile, run} from './config.json';
import fs from 'fs';
import {getTeamList} from './teamList';
import {getLogBox} from './logBox.js';

let repos;
let screen;
export function createRepos(screen, header) {
    if (!repos)
        create(screen, header);
    return repos;
}
export function getRepos() {
    return repos;
}
function create(scrn, header) {
    screen = scrn;
    repos = blessed.listtable({
        parent: screen,
        top: '0',
        left: '16%',
        width: '83%',
        height: '50%',
        tags: true,
        scrollable: true,
        label: 'Repos',
        border: {
            type: 'line',
            left:false,
            right:false,
            top:false,
            bottom:false
        },
        keys: true,
        vi: true,
        style: {
            header: {
                fg: 'cyan',
                border: {
                    type: 'line',
                    bottom: true
                },
                bold: true
            },
            cell: {
                fg: 'white',
                bg: 'black',
                selected: {
                    bg: 'green'
                }
            },
            border: {
                fg: 'magenta'
            }
        },
        data: header
    });
    repos.on('select', async (obj, id) => {
        getLogBox().focus();
        if (id) {
            const teamList = getTeamList();
            const team = teamList.getItem(teamList.selected);
            const row = repos.rows[id];
            const dir = await download(row, team).catch(e => null);
            if (dir) {
                log('Running project ' + row[1]);
                log(`cd "${dir}" ; ${compile}`)
                exec(`cd "${dir}" ; ${compile}`, (error, stdout, stderr) => {
                    if (stderr) {
                        log(stderr);
                    }
                    log(stdout);
                    if (error) {
                        setColor(obj, team, 'red')
                    } else {
                        log(`cd "${dir}" ; ${run}`)
                        exec(`cd "${dir}" ; ${run}`, (error, stdout, stderr) => {
                            if (stderr) {
                                log(stderr);
                            }
                            if (error) {
                                setColor(obj, team, 'red')
                            } else {
                                log(`cd "${dir}" ; ./${stdout}`);
                                exec(`cd "${dir}" ; ./${stdout}`, (error, stdout, stderr) => {
                                    if (stderr) {
                                        log(stderr);
                                    }
                                    log(stdout);
                                    if (error) {
                                        setColor(obj, team, 'red')
                                    } else {
                                        setColor(obj, team, 'blue');
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                setColor(obj, team, 'red')
            }
        }
    });
}

function setColor(obj, team, color) {
    obj.style.bg = color;
    team.style.bg = color;
    screen.render();
}

function download (row, team) {
    return new Promise((resolve, reject) => {
        log('Checking out: ')
        log(row[1]);
        const idx = username.length-username.indexOf('@')+4;
        const httpCred = `${row[3].slice(0, idx)}:${password}${row[3].slice(idx)}`;
        log(httpCred);
        const dir = `./repos/${team.getContent()}+${row[0]}`
        if (!fs.existsSync('./repos')) {
            fs.mkdir('./repos');
        }
        if (fs.existsSync(dir)) {
            exec(`cd "${dir}" ; git pull ${httpCred} master`, (error, stdout, stderr) => {
                if (stderr) {
                    log(stderr)
                }
                log(stdout);
                log('Done')
                if (error) {
                    return reject(error);
                }
                return resolve(dir);
            });
        } else {
            exec(`git clone ${httpCred} "${dir}"`, (error, stdout, stderr) => {
                if (stderr) {
                    log(stderr)
                }
                log(stdout);
                log('Done')
                if (error) {
                    return reject(error);
                }
                return resolve(dir);
            });
        }
    });
}
function deleteFolderRecursive (path) {
    if( fs.existsSync(path)  ) {
        fs.readdirSync(path).forEach((file,index) => {
              const curPath = path + "/" + file;
              if(fs.lstatSync(curPath).isDirectory()) { // recurse
                  deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                  }
        });
        fs.rmdirSync(path);
    }
}
