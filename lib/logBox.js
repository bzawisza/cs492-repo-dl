import stringify from 'json-stringify-safe';
import blessed from 'blessed';
import {getTeamList} from './teamList';

let logBox;
export function createLogBox(screen) {
    if (!logBox)
        create(screen);
    return logBox;
}

export function getLogBox() {
    return logBox;
}

function create(screen) {
    logBox = blessed.box({
        parent: screen,
        top: '50%',
        left: '0',
        width: '100%',
        height: '50%',
        tags: true,
        content: '',
        scrollable: true,
        label:'Logs',
        keys: true,
        border: {
            type: 'line',
            left:true,
            right:true,
            top:true,
            bottom:true
        },
        style: {
            fg: 'white',
            bg: 'black',
            border: {
                fg: 'magenta'
            }
        }
    });
    logBox.key('j', () => {
        const lines = logBox.getLines().length;
        const h = (logBox.childBase || 0) + logBox.height - logBox.iheight
            , i = Math.min(h, logBox._clines.length)
            , base = logBox._clines.rtof[i - 1];
        const min = Math.max(base, logBox.getScroll());
        const scroll = Math.min(lines, min+1)
        logBox.scrollTo(scroll);
    });
    logBox.key('k', () => {
        const base = logBox._clines.rtof[logBox.childBase || 0];
        const line = Math.min(base, logBox.getScroll());
        const scroll = Math.max(0, line-1);
        logBox.scrollTo(scroll);
    });
    logBox.key('enter', () => {
        getTeamList().focus();
    });
}

export function log(obj) {
    if (obj !== null && obj !== undefined) {
        let str = obj;
        if (typeof obj != "string")
            str = stringify(obj, null, 2);
        str.split('\n').forEach(l => logBox.pushLine(l));
        logBox.scrollTo(logBox.getLines().length);
    }
}
